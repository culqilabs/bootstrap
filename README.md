## Bootstrap

Librería para uso común de los microservicios en Evolution, tiene los siguientes Features:

- conexión a Kafka
- conexión a Redis
- Logging
- HealtCheck
- Validacion de schema del request usando Joi
- Creación del objeto Application, inyectando middlewares de manejo de errores con su respectivo logueo.
