import request from "supertest";
import {} from "../src/index";
import { getMeta } from "../src/helpers/messages";

const data = {
  meta: {
    serviceId: "123",
    timestamp: "2019-09-15T11:13:20.123Z-0500",
    request: {
      traceId: "456",
      ipAddress: "127.0.0.1",
      userId: "1",
    },
  },
  data: {
    documentNumber: "44360655",
  },
};

afterAll((done) => setImmediate(done));

describe("features", () => {
  test("api", async () => {
    const config = {
      serviceId: "123",
      timezone: "America/Lima",
      dateFormat: "yyyy-MM-DDTHH:mm:ss.SSS[Z]ZZ",
    };

    const request = {
      traceId: "456",
      ipAddress: "127.0.0.1",
    };

    const message = getMeta(config, request);
    console.log(message);

    // const { body } = await request(null)
    //   .post("/api/test")
    //   .set("Content-Type", "application/json")
    //   .send(data);
    // expect(body?.data?.msg).toBe("Hola test");
  });
  // test("log", async () => {
  //   const { body } = await request(null)
  //     .post("/api/log")
  //     .set("Content-Type", "application/json")
  //     .send(data);
  //   expect(body?.meta?.request).toMatchObject({
  //     traceId: "456",
  //     ipAddress: "127.0.0.1",
  //     userId: "1",
  //   });
  // });
});
