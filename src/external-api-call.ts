import axios from "axios";
import nanoid from "nanoid";
import request from "request";
import { Config } from "./config";
import { LoggerFactory } from "./logger-factory";
import { getMeta } from "./helpers/messages";
import * as cfg from "./types/config";
import { BusinessError, InternalError, ExposeError, InvalidArgumentError } from "./errors";
import ErrorTypes from "./helpers/error-types";
import { Logger } from "log4js";

export type Method = "get" | "GET" | "delete" | "DELETE" | "head" | "HEAD" | "options" | "OPTIONS" | "post" | "POST" | "put" | "PUT" | "patch" | "PATCH" | "link" | "LINK" | "unlink" | "UNLINK";

export type ResponseType = "arraybuffer" | "blob" | "document" | "json" | "text" | "stream";

export interface BasicCredentials {
  username: string;
  password: string;
}

export interface ProxyConfig {
  host: string;
  port: number;
  auth?: {
    username: string;
    password: string;
  };
  protocol?: string;
}

export type ExternalApiCallConfig = {
  url: string;
  method: Method;
  headers?: Record<string, unknown>;
  params?: Record<string, unknown>;
  data?: Record<string, unknown>;
  timeout?: number;
  withCredentials?: boolean;
  auth?: BasicCredentials;
  responseType?: ResponseType;
  proxy?: ProxyConfig;
  request?: cfg.MetaRequest;
};

const byAxios = async (logger: Logger, config: Config, eConfig: ExternalApiCallConfig, originId: string, sessionId: string) => {
  const body = eConfig.request
    ? {
        meta: { ...getMeta(config.me.application, eConfig.request), originId, sessionId },
        data: eConfig.data || {},
      }
    : null;

  const destination = eConfig.url;

  logger.trace(body, body?.meta, destination, destination);

  try {
    const { data: response } = await axios({ ...eConfig });

    if (eConfig.responseType && response.meta) {
      logger.trace(response, response.meta, destination, destination);

      if (response.error) {
        const { error } = response;

        if (error.type === ErrorTypes.INTERNAL) {
          throw new InternalError(error.message, error.status, { translate: false });
        }

        if (error.type === ErrorTypes.BUSINESS) {
          throw new BusinessError(error.message, error.status, { translate: false });
        }

        if (error.type === ErrorTypes.EXPOSE) {
          throw new ExposeError(error.message, error.status, { translate: false });
        }

        if (error.type === ErrorTypes.INVALID_ARGUMENT) {
          // se supone que ya está reemplazado
          throw new InvalidArgumentError(error.message, error.status, { translate: false });
        }

        throw new Error(error.message);
      }

      return response.data;
    }

    logger.trace(response, null, destination, destination);

    return response;
  } catch (error) {
    logger.trace(error.response?.data ?? error.message, null, destination, destination);
    throw error;
  }
};

const byRequest = async (logger: any, config: Config, eConfig: ExternalApiCallConfig, originId: string, sessionId: string) => {
  const asyncRequest = async (options: any) => {
    return new Promise((resolve, reject) => {
      try {
        request(options, (error: any, response: any, body: any) => resolve({ error, response, body }));
      } catch (error) {
        reject(error);
      }
    });
  };

  const meta = { ...getMeta(config.me.application, eConfig.request), originId, sessionId };

  const destination = eConfig.url;

  logger.trace(eConfig.data, meta, destination, destination);

  const options = {
    url: eConfig.url,
    method: eConfig.method,
    proxy: `${eConfig.proxy.protocol}://${eConfig.proxy.host}:${eConfig.proxy.port}`,
    headers: eConfig.headers,
    strictSSL: eConfig.withCredentials,
    tunel: true,
    json: eConfig.data,
  };

  try {
    let response: any;
    const responseRequest: any = await asyncRequest(options);
    if (responseRequest.error) throw responseRequest.error;
    else if (responseRequest.body) response = responseRequest.body;

    logger.trace(response, null, destination, destination);

    return response;
  } catch (error) {
    logger.trace(error.response?.data ?? error.message, null, destination, destination);
    throw error;
  }
};

export const ExternalApiCall = (config: Config, loggerFactory: LoggerFactory) => {
  const logger = loggerFactory.getLogger("HTTP");

  const originId = config.me.application.serviceId;

  return async (eConfig: ExternalApiCallConfig): Promise<any | never> => {
    const sessionId = nanoid();

    if (eConfig.proxy?.host) {
      return byRequest(logger, config, eConfig, originId, sessionId);
    } else {
      return byAxios(logger, config, eConfig, originId, sessionId);
    }
  };
};
