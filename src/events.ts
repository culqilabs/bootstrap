import kafka from "kafkajs";
import nanoid from "nanoid";
import { Config } from "./config";
import { HealthCheck } from "./health-check";
import { getConsumer as Consumer, getKafka as Kafkajs, getProducer as Producer } from "./helpers/kafkajs";
import { LoggerFactory } from "./logger-factory";

/**
 * @deprecated Por favor utilizar Queue.
 *
 * Events maneja la configuración y conexión hacia kafka
 */
export class Events {
  private id: string;
  private _kafkajsProducer: kafka.Producer;
  private _kafkajsConsumer: kafka.Consumer;

  /**
   * Configura y crea un objeto consumer o producer en kafka, inyectando log y healthcheck.
   *
   * @param config según la config se crea un objeto consumer o producer.
   * @param loggerFactory
   * @param healthCheck
   */
  constructor(private config: Config, private loggerFactory: LoggerFactory, private healthCheck?: HealthCheck) {
    this.id = nanoid();

    const kafka = Kafkajs(config.me.kafka);

    // kafka producer
    if (this.config.me.kafka) {
      const name = "kafka_producer";
      if (healthCheck) {
        healthCheck.registerService(name);
      }
      this._kafkajsProducer = Producer(kafka, this.loggerFactory, name, this.healthCheck);
    }

    // kafka consumer
    if (this.config.me.kafka && this.config.me.kafkaConsumer) {
      const name = "kafka_consumer";
      if (healthCheck) {
        healthCheck.registerService(name);
      }
      this._kafkajsConsumer = Consumer(kafka, this.config.me.kafkaConsumer, this.loggerFactory, name, this.healthCheck);
    }
  }

  get kafkaProducer() {
    return this._kafkajsProducer;
  }

  get kafkaConsumer() {
    return this._kafkajsConsumer;
  }
}
