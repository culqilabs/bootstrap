import axios from "axios";
import urlJoin from "url-join";
import nanoid from "nanoid";
import { Config } from "./config";
import { BusinessError, InternalError, ExposeError, InvalidArgumentError } from "./errors";
import ErrorTypes from "./helpers/error-types";
import { getMeta } from "./helpers/messages";
import { LoggerFactory } from "./logger-factory";
import * as cfg from "./types/config";

/**
 * API CALL abstrae la comunicación entre microservicios usando Axios (HTTP) como transporter
 * siguiendo el formato establecido en la documentación
 *
 * https://culqilabs.gitlab.io/docs/docs/microservices/index#meta
 *
 * TODO: API CALL debe soportar múltiples forma de transporters (HTTP, gRPC)
 *
 * Ejemplo:
 *
 * 1. Crear un archivo api-call.ts donde se le pase la config a la función:
 *
 * import { ApiCall } from "@culqi/bootstrap";
 * import config from "./config";
 * import loggerFactory from "./logger-factory";
 *
 * export default ApiCall(config, loggerFactory);
 *
 * 2. Crear en tu config (ie. config/development.toml) las rutas del microservicio a usar:
 *
 * [providers.bin]
 * url= "http://ms-bin:3000"
 * enrichBin = "/bin/internal/lookup"
 *
 * 3. Crear un file para llamar al api - providers/ms.providers.ts
 *
 * const api = apiCall(config.me.providers.bin.url);
 * const data = await api(config.me.providers.bin.enrichBin;, data, request);
 *
 * Signature:
 *
 * (url: string, data: any, request: cfg.MetaRequest, asTuple = false) => promise<any>
 *
 * TODO: Se requiere el Request obtener el TRACE_ID deberiamos eliminar esta dependencia
 *
 * @param config
 * @param loggerFactory
 */
export const ApiCall = (config: Config, loggerFactory: LoggerFactory) => {
  const logger = loggerFactory.getLogger("HTTP");

  return (baseURL: string) => {
    const api = axios.create({
      baseURL,
      headers: { "Content-Type": "application/json" },
      responseType: "json",
    });

    const destinationId = baseURL.match(/(\/\/)+.+:/g)[0]?.replace(/(\/\/)|:/g, "");

    return async (url: string, data: any, request: cfg.MetaRequest, asTuple = false): Promise<any> => {
      const destinationPath = urlJoin(baseURL, url)
        .match(/:\d{4}.*/g)[0]
        ?.replace(/:\d{4}/g, "")
        .replace(/(\/\/)|:/g, "/");
      const sessionId = nanoid();
      const originId = config.me.application.serviceId;
      const body = {
        meta: { ...getMeta(config.me.application, request), originId, sessionId },
        data,
      };
      logger.trace(body, body.meta, destinationId, destinationPath);
      const {
        data: response,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        data: { data: data$, error, meta },
      } = await api.post(url, body);
      logger.trace(response, response?.meta, destinationId, destinationPath);
      if (asTuple) {
        if (error) {
          if (error.type === ErrorTypes.INTERNAL) {
            return [new InternalError(error.message, error.status, { translate: false }), null];
          }
          if (error.type === ErrorTypes.BUSINESS) {
            return [new BusinessError(error.message, error.status, { translate: false }), null];
          }
          if (error.type === ErrorTypes.EXPOSE) {
            return [new ExposeError(error.message, error.status, { translate: false }), null];
          }
          if (error.type === ErrorTypes.INVALID_ARGUMENT) {
            // se supone que ya está reemplazado
            return [new InvalidArgumentError(error.message, error.status, { translate: false }), null];
          }
          return [new Error(error.message), null];
        }
        return [null, data$];
      } else {
        if (error) {
          if (error.type === ErrorTypes.INTERNAL) {
            throw new InternalError(error.message, error.status, { translate: false });
          }
          if (error.type === ErrorTypes.BUSINESS) {
            throw new BusinessError(error.message, error.status, { translate: false });
          }
          if (error.type === ErrorTypes.EXPOSE) {
            throw new ExposeError(error.message, error.status, { translate: false });
          }
          if (error.type === ErrorTypes.INVALID_ARGUMENT) {
            // se supone que ya está reemplazado
            throw new InvalidArgumentError(error.message, error.status, { translate: false });
          }
          throw new Error(error.message);
        }
        return data$;
      }
    };
  };
};
