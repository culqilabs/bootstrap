import { Schema } from "@hapi/joi";
import { Response } from "express";

// validacion de schemas JSON usando libreria JOI
// https://github.com/hapijs/joi

/**
 * Interceptor que valida el request de una forma standard.
 * En caso de error lo emite usando $internalError para mantener consistencia.
 *
 * Ejemplo:
 *
 * @JoiValidation(schemas.createCard)
 * async create(req: Request, res: Response): Promise<any> {
 *  return await CardController.execute(res, CardsService.create);
 * }
 *
 * @param schema
 */
export function JoiValidation(schema: Schema) {
  return function(target: Record<string, any>, propertyName: string, propertyDescriptor: PropertyDescriptor): PropertyDescriptor {
    const method = propertyDescriptor.value;
    propertyDescriptor.value = function(...args: any[]) {
      const res = args[1] as Response;
      const { error } = schema.validate(res.locals.data);
      if (error) {
        return res.$internalError({
          message: res.$t(error.message) as string,
          status: 400,
        });
      }
      return method.apply(this, args);
    };
    return propertyDescriptor;
  };
}

export function JoiValidationExpose(schema: Schema) {
  return function(target: Record<string, any>, propertyName: string, propertyDescriptor: PropertyDescriptor): PropertyDescriptor {
    const method = propertyDescriptor.value;
    propertyDescriptor.value = function(...args: any[]) {
      const res = args[1] as Response;
      const { error } = schema.validate(res.locals.data);
      if (error) {
        let message;
        try {
          message = JSON.parse(error.message);
        } catch (e) {
          message = error.message;
        }
        return res.$exposeError({
          message,
          status: 400,
        });
      }
      return method.apply(this, args);
    };
    return propertyDescriptor;
  };
}

export const JoiCall = (schemas: Record<string, Schema>) => () => {
  return function(target: Record<string, any>, propertyName: string, propertyDescriptor: PropertyDescriptor): PropertyDescriptor {
    const method = propertyDescriptor.value;
    propertyDescriptor.value = function(...args: any[]) {
      if (schemas[propertyName]) {
        const res = args[1] as Response;
        const { error } = schemas[propertyName].validate(res.locals.data);
        if (error) {
          return res.$internalError({
            message: res.$t(error.message) as string,
            status: 400,
          });
        }
      }
      return method.apply(this, args);
    };
    return propertyDescriptor;
  };
};
