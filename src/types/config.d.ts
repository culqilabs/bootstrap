import log4js from "log4js";
import { CompressionTypes } from "kafkajs";
import ErrorTypes from "../helpers/error-types";

export type HealthCheck = {
  signal: string
  timeout: number
}

export type i18n = {
  dir: string
  locales: string[]
  defaultLocale: string
  objectNotation: boolean
}

export type GenericSuccess = {
  meta: Meta
  data: any
}

export type ExposeError = {
  meta: Meta
  error: {
    message: string | unknown
    status: number
    type: ErrorTypes
  }
}

export type GenericError = {
  meta: Meta
  error: {
    message: string | unknown
    status: number
    type: ErrorTypes
  }
}

export type MetaRequest = {
  traceId: string
  ipAddress: string
  userId?: string
  publicKey?: string
  secretKey?: string
  ticketId?: string
  host?: string
  userAgent?: string
  source?: string
}

export type Meta = {
  serviceId: string
  timestamp: string
  request: MetaRequest
  sessionId?: string
  originId?: string
}

export type Application = {
  serviceId?: string
  timezone?: string
  dateFormat?: string
  port?: number
}

export type Config = {
  i18n: i18n,
  application: Application
  log4js: log4js.Configuration
  mongoose?: {
    [name: string]: Mongoose
  }
  ioredis?: {
    [name: string]: Ioredis
  }
  kafka?: Kafkajs
  kafkaConsumer?: KafkaConsumerConfig
  healthCheck: HealthCheck
  collections: Collections
  queue: {
    [key: string]: Queue
  }
}

export type MongooseOptions = {
  sslCA?: string[]
  [name: string]: any
}

export type Mongoose = {
  uri: string
  options: MongooseOptions
}

export type Ioredis = {
  uri: string
}

export type KafkaConsumerConfig = {
  groupId: string
  topics: string[]
}

export type Kafkajs = {
  clientId: string
  brokers: string[]
  ssl?: any
}

export type Collections = {
  names: string[]
}

export type QueueConsumerOptions = {
  fromBeginning: boolean
  raw: boolean
  schemaRegistry?: {
    host: string
  }
}

export type QueueConsumer = {
  [name: string]: QueueConsumerOptions
}

export type QueueProducer = {
  [name: string]: {
    acks: number
    timeout: number
    compression: CompressionTypes
  }
}

export type Queue = {
  // clientId: string
  brokers: string[]
  ssl?: any
  producers: QueueProducer
  consumers: QueueConsumer
  autoCommit: boolean
}
