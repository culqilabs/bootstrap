declare namespace Express {
  export type $BusinessError = {
    message: string
    status: number
    options?: {
      replacements?: Readonly<Record<string, unknown>>
      translate?: boolean
      expose?: boolean
    }
  }

  export type $InternalError = {
    message: string
    status: 511 | 507 | 505 | 504 | 503 | 502 | 501 | 500 | 431 | 429 | 428 | 424 | 423 | 422 | 420 | 419 | 418 | 417 | 416 | 415 | 414 | 413 | 412 | 411 | 410 | 409 | 408 | 407 | 406 | 405 | 404 | 403 | 402 | 401 | 400
    options?: {
      replacements?: Readonly<Record<string, unknown>>
      translate?: boolean
      expose?: boolean
    }
  }

  export type $ExposeError = {
    message: Record<string, unknown>
    status: 511 | 507 | 505 | 504 | 503 | 502 | 501 | 500 | 431 | 429 | 428 | 424 | 423 | 422 | 420 | 419 | 418 | 417 | 416 | 415 | 414 | 413 | 412 | 411 | 410 | 409 | 408 | 407 | 406 | 405 | 404 | 403 | 402 | 401 | 400
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean
      expose?: boolean
    }
  }

  export type $InvalidArgumentError = {
    message: string;
    status: 511 | 507 | 505 | 504 | 503 | 502 | 501 | 500 | 431 | 429 | 428 | 424 | 423 | 422 | 420 | 419 | 418 | 417 | 416 | 415 | 414 | 413 | 412 | 411 | 410 | 409 | 408 | 407 | 406 | 405 | 404 | 403 | 402 | 401 | 400
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean
      expose?: boolean
    }
  }

  export interface Response {
    $t: (phrase: string, replacements?: Record<string, any>) => Record<string, string> | string
    $tn: (phrase: string, replacements?:  Record<string, any>) => Record<string, string> | string
    $success: (data: Record<string, any>) => void
    $businessError: (error: $BusinessError) => void
    $internalError: (error: $InternalError) => void
    $exposeError: (error: $ExposeError) => void
    $invalidArgumentError: (error: $InvalidArgumentError) => void
    $logTrace: (message: any) => void
    $logDebug: (message: any) => void
    $logInfo: (message: any) => void
    $logWarn: (message: any) => void
    $logError: (message: any) => void
    $logFatal: (message: any) => void
  }
}
