import { Request, Response } from "express";

declare module "express-laravel-router" {
  export type Router = {
    group: (uri: string, router: (router: Router) => void) => void
    post: (uri: string, action: (req: Request, res: Response) => void) => void
  }

  export function createRouter(app: any, mapActionToHandler?: any): Router;

  export function laravelToExpress(uri: any, patterns: any): any;
  
  export function paramsFromUri(uri: any): any;
  
  export function uriWithParams(uri: any, params: any, patterns: any, options: any): any;
}
