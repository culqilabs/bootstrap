import BaseJoi from "@hapi/joi";
import Extension from "@hapi/joi-date";
import { NextFunction, Request, Response } from "express";
import httpStatusCodes from "http-status-codes";

const Joi = BaseJoi.extend(Extension);

const schema = Joi.object()
  .keys({
    meta: Joi.object()
      .keys({
        originId: Joi.string().optional(),
        sessionId: Joi.string().optional(),
        serviceId: Joi.string().required(),
        timestamp: Joi.date()
          .format("YYYY-MM-DDTHH:mm:ss.SSSZZZ")
          .required(),
        request: Joi.alternatives()
          .try(
            Joi.object()
              .keys({
                traceId: Joi.string()
                  .trim()
                  .required(),
                ipAddress: Joi.string()
                  .ip()
                  .required(),
                userId: Joi.string()
                  .trim()
                  .required(),
                host: Joi.string()
                  .trim()
                  .optional(),
                userAgent: Joi.string()
                  .trim()
                  .optional(),
                method: Joi.string()
                  .trim()
                  .optional(),
              })
              .required(),
            Joi.object()
              .keys({
                traceId: Joi.string()
                  .trim()
                  .required(),
                ipAddress: Joi.string()
                  .ip()
                  .required(),
                publicKey: Joi.string()
                  .trim()
                  .required(),
                host: Joi.string()
                  .trim()
                  .optional(),
                userAgent: Joi.string()
                  .trim()
                  .optional(),
                method: Joi.string()
                  .trim()
                  .optional(),
              })
              .required(),
            Joi.object()
              .keys({
                traceId: Joi.string()
                  .trim()
                  .required(),
                ipAddress: Joi.string()
                  .ip()
                  .required(),
                secretKey: Joi.string()
                  .trim()
                  .required(),
                host: Joi.string()
                  .trim()
                  .optional(),
                userAgent: Joi.string()
                  .trim()
                  .optional(),
                method: Joi.string()
                  .trim()
                  .optional(),
              })
              .required(),
            Joi.object()
              .keys({
                traceId: Joi.string()
                  .trim()
                  .required(),
                ipAddress: Joi.string()
                  .ip()
                  .required(),
                ticketId: Joi.string()
                  .trim()
                  .required(),
                host: Joi.string()
                  .trim()
                  .optional(),
                userAgent: Joi.string()
                  .trim()
                  .optional(),
                method: Joi.string()
                  .trim()
                  .optional(),
              })
              .required(),
            Joi.object()
              .keys({
                traceId: Joi.string()
                  .trim()
                  .required(),
                ipAddress: Joi.string()
                  .ip()
                  .required(),
                source: Joi.string()
                  .trim()
                  .required(),
                host: Joi.string()
                  .trim()
                  .optional(),
                userAgent: Joi.string()
                  .trim()
                  .optional(),
                method: Joi.string()
                  .trim()
                  .optional(),
              })
              .required(),
          )
          .required(),
      })
      .required(),
    data: Joi.object().required(),
  })
  .required();

const ids = ["userId", "publicKey", "secretKey", "ticketId", "source"];

export default (req: Request, res: Response, next: NextFunction): any => {
  const { value, error } = schema.validate(req.body);
  if (error) {
    return res.$internalError({
      message: error.details[0].message,
      status: httpStatusCodes.BAD_REQUEST,
    });
  }
  res.locals.data = value.data;
  res.locals.meta = value.meta;
  const request: Record<string, unknown> = value.meta.request;
  const found = ids.find((id) => (request[id] as string)?.length > 0);
  res.locals.authId = `${found}:${request[found]}`;
  return next();
};
