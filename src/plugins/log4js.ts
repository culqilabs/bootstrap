import log4js, { Log4js, LoggingEvent } from "log4js";
import * as cfg from "../types/config";

// logEvent?.data[0] => data
// logEvent?.data[1] => meta
//                      meta.request
//                      meta.request.traceId
//                      meta.sessionId
//                      meta.originId
// logEvent?.data[2] => destinationId
// logEvent?.data[3] => destinationPath
export const auditLayout = (config: cfg.Application) => () => (logEvent: LoggingEvent) =>
  JSON.stringify({
    timestamp: Date.now(),
    category: logEvent.categoryName,
    level: logEvent.categoryName === "HTTP" ? null : logEvent.level.levelStr,
    serviceId: config.serviceId,
    originId: logEvent?.data[1]?.originId ?? null,
    destinationId: logEvent?.data[2] ?? null,
    destinationPath: logEvent?.data[3] ?? null,
    traceId: logEvent?.data[1]?.request?.traceId ?? null,
    sessionId: logEvent?.data[1]?.sessionId ?? null,
    eventId: null,
    message: JSON.stringify(logEvent?.data[0]),
  });

export default (config: cfg.Config): Log4js => {
  log4js.addLayout("audit", auditLayout(config.application));
  log4js.configure(config.log4js);
  return log4js;
};
