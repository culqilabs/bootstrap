import { Express } from "express";
import i18n from "i18n";
import path from "path";

export const nestedReplacements = (phrase: Record<string, unknown> | string, replacements: Record<string, unknown>) => {
  return typeof phrase === "string"
    ? phrase
    : Object.keys(phrase).reduce((acc, cur) => {
        acc[cur] = Object.keys(replacements).reduce((a, c) => (typeof a === "string" ? a.replace(`{{${c}}}`, replacements[c] as string) : a), phrase[cur]);
        return acc;
      }, {} as Record<string, unknown>);
};

/**
 * Configura los archivos de idioma usados en los microservicios
 *
 * @param app
 * @param dir
 * @param config
 */
export default (app: Express, dir = "locales", { locales = ["es"], defaultLocale = "es", objectNotation = true } = {}): void => {
  i18n.configure({
    locales,
    defaultLocale,
    directory: path.resolve(dir),
    objectNotation,
    api: {
      __: "$t",
      __n: "$tn",
    },
  });
  app.use(i18n.init);
};
