import { createTerminus } from "@godaddy/terminus";
import debugLib from "debug";
import { EventEmitter } from "events";
import { Server } from "http";
import { Config } from "./config";

const debug = debugLib("bootstrap:hc");

/**
 * HealthCheck configura terminus para que kubernetes sepa que un microservicio está listo y si está vivo.
 *
 * Para más información sobre terminus revisar https://github.com/godaddy/terminus
 *
 * file: /config/local.toml
 * [healthCheck]
 * signal = "SIGINT"
 * timeout = 3000
 *
 * @param config usa la parte HealthCheck de config.
 */
export class HealthCheck {
  private _services: Map<string, boolean> = new Map();
  private _myEmitter = new EventEmitter();
  private _shutdowns: (() => Promise<any>)[] = [];

  constructor(private config: Config) {}

  public registerService(name: string): void {
    if (!this._services.has(name)) {
      debug(`registerService > ${name}`);
      this._services.set(name, false);
      this._myEmitter.on(name, (status: boolean) => this._services.set(name, status));
    }
  }

  public registerShutdown(fn: () => Promise<any>): void {
    this._shutdowns.push(fn);
  }

  public emit(name: string, status: boolean): void {
    debug(`emit > ${name} - ${status}`);
    this._myEmitter.emit(name, status);
  }

  public listen(server: Server) {
    createTerminus(server, {
      ...this.config.me.healthCheck,
      healthChecks: {
        "/_health/liveness": () => Promise.resolve(),
        "/_health/readiness": () => {
          if (Array.from(this._services.values()).every(Boolean)) return Promise.resolve();
          return Promise.reject(new Error("not ready"));
        },
      },
      onSignal: () => Promise.all(this._shutdowns.map((fn) => fn())),
    });
  }
}
