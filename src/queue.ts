import { EventEmitter } from "events";
import debugLib from "debug";
import { CompressionTypes, Kafka, Consumer } from "kafkajs";
import { SchemaRegistry } from "@kafkajs/confluent-schema-registry";
import { Logger } from "log4js";
import nanoid from "nanoid";
import { Config } from "./config";
import { HealthCheck } from "./health-check";
import { getMeta } from "./helpers/messages";
import { LoggerFactory } from "./logger-factory";
import * as cfg from "./types/config";
import * as fs from "fs";
import * as path from "path";

const debug = debugLib("bootstrap:queue");

/**
 * Construyen productores de eventos
 *
 * @param kafka
 * @param config
 * @param loggerFactory
 * @param healthCheck
 * @param em
 */
function buildProducers(kafka: Kafka, config: Config, queue: cfg.Queue, name: string, loggerFactory: LoggerFactory, healthCheck: HealthCheck, em: EventEmitter, emx?: EventEmitter): void {
  const logger = loggerFactory.getLogger("QUEUE");
  const loggerShutdown = loggerFactory.getLogger("shutdown");

  const producerName = `${name}/kafka_producer`;
  const originId = config.me.application.serviceId;

  healthCheck.registerService(producerName);

  const producer = kafka.producer();

  // Config healthCheck
  const { CONNECT, DISCONNECT } = producer.events;
  producer.on(CONNECT, () => {
    healthCheck.emit(producerName, true);
    emx?.emit(`${name}/onConnect`, true);
  });
  producer.on(DISCONNECT, () => {
    healthCheck.emit(producerName, false);
    emx?.emit(`${name}/onDisconnect`, true);
  });
  healthCheck.registerShutdown(() => {
    loggerShutdown.info(`shutdown connection: ${producerName}`);
    return producer.disconnect();
  });

  producer.connect().then(() => {
    Object.entries(queue.producers).forEach(([topic, options]) => {
      //TODO: Refactorizar interface y darle nombre
      debug(`💡 registrando/producer -> ${name}/${topic}`);
      em.on(`${name}/${topic}`, ({ event, request, data }: { event: string; request: cfg.MetaRequest; data: Record<string, any> }) => {
        debug(`🤞 emitiendo/producer -> ${name}/${topic}`);
        const sessionId = nanoid();

        const content = {
          meta: { ...getMeta(config.me.application, request), originId, sessionId },
          data,
        };

        logger.trace(content, content?.meta, `${name}/${topic}`, `${name}/${topic}`);

        producer
          .send({
            topic,
            messages: [
              {
                value: JSON.stringify({
                  event,
                  content,
                }),
              },
            ],
            acks: options.acks || -1,
            timeout: options.timeout || 30000,
            compression: options.compression || CompressionTypes.None,
          })
          .then(() => {
            debug(`🆗 ok/producer -> ${name}/${topic}`);
          })
          .catch((err) => {
            debug(`⛔ ko/producer -> ${name}/${topic}`, err.message);
          });
      });
    });
  });
}

async function registrarConsumer(map: Map<string, Array<(data: any) => void>>, logger: Logger, queue: cfg.Queue, name: string, consumer: Consumer, topic: string, options: cfg.QueueConsumerOptions): Promise<void> {
  const hasRegistry = !!options.schemaRegistry?.host;

  if (hasRegistry) {
    const { certRootPath, ca, caEnvVar } = queue.ssl;
    process.env[`${caEnvVar}`] = `${path.join(certRootPath, ca)}`;
  }
  const registry =
    hasRegistry &&
    new SchemaRegistry({
      host: options.schemaRegistry?.host,
    });

  await consumer.subscribe({ topic, fromBeginning: options.fromBeginning || false });

  await consumer.run({
    autoCommit: queue.autoCommit || true,
    eachMessage: async ({ topic, message }) => {
      debug(`🤞 consumer -> ${name}/${topic}`);
      if (map.has(`${name}/${topic}`)) {
        let response: Data;
        try {
          if (hasRegistry) {
            const decodedKey = await registry.decode(message.key);
            const decodedValue = await registry.decode(message.value);

            logger.trace(response, null, `${name}/${topic}`, `${name}/${topic}`);

            map.get(`${name}/${topic}`).forEach((fns) => fns.call(null, { key: decodedKey, value: decodedValue }));
          } else if (options.raw) {
            logger.trace(response, null, `${name}/${topic}`, `${name}/${topic}`);

            map.get(`${name}/${topic}`).forEach((fns) => fns.call(null, message));
          } else {
            response = JSON.parse(message.value.toString());
            logger.trace(response, response?.content?.meta, `${name}/${topic}`, `${name}/${topic}`);

            map.get(`${name}/${topic}`).forEach((fns) => fns.call(null, response));
          }
          debug(`👍 consumer -> ${name}/${topic}`);
        } catch (err) {
          debug(`👎 consumer -> ${name}/${topic}`, err.message);
          logger.error(
            {
              message: err.message,
              stack: err.stack,
            },
            {
              request: response && response?.content?.meta?.request,
            },
          );
        }
      }
    },
  });
}

export const EmitterExternalEvents = Object.freeze({
  DISCONNECT: "consumer.disconnect",
  CRASH: "consumer.crash",
  REQUEST_TIMEOUT: "consumer.requesttimeout",
});

const EmitterExternalEventsAsArray = Object.values(EmitterExternalEvents);

// TODO: Refactorizar esta interface
type Data = { event: string; content: { meta: cfg.Meta; data: any } };

/**
 * Construyen consumers de eventos
 *
 * @param kafka
 * @param config
 * @param loggerFactory
 * @param healthCheck
 * @param map
 */
function buildConsumers(kafka: Kafka, config: Config, queue: cfg.Queue, name: string, loggerFactory: LoggerFactory, healthCheck: HealthCheck, map: Map<string, Array<(data: any) => void>>, emx?: EventEmitter) {
  const logger = loggerFactory.getLogger("QUEUE");
  const loggerApp = loggerFactory.getLogger("app");
  const loggerShutdown = loggerFactory.getLogger("shutdown");

  const consumerName = `${name}/kafka_consumer`;
  healthCheck.registerService(consumerName);
  const consumer = kafka.consumer({ groupId: `group/${name}/${config.me.application.serviceId}` });

  // Config healthCheck
  const { CONNECT, DISCONNECT, HEARTBEAT, CRASH, STOP, REQUEST_TIMEOUT } = consumer.events;
  consumer.on(CONNECT, () => healthCheck.emit(consumerName, true));
  consumer.on(DISCONNECT, () => {
    healthCheck.emit(consumerName, false);
    emx?.emit(`${name}/${EmitterExternalEvents.DISCONNECT}`, false);
  });
  consumer.on(HEARTBEAT, () => healthCheck.emit(consumerName, true));
  consumer.on(CRASH, (err: Error) => {
    loggerApp.error(err);
    healthCheck.emit(consumerName, false);
    emx?.emit(`${name}/${EmitterExternalEvents.CRASH}`, err);
    setTimeout(() => process.exit(1), 1000);
  });
  consumer.on(STOP, () => healthCheck.emit(consumerName, false));
  consumer.on(REQUEST_TIMEOUT, () => {
    healthCheck.emit(consumerName, false);
    emx?.emit(`${name}/${EmitterExternalEvents.REQUEST_TIMEOUT}`, false);
  });
  healthCheck.registerShutdown(() => {
    loggerShutdown.info(`shutdown connection: ${consumerName}`);
    return consumer.disconnect();
  });

  consumer.connect().then(() => Promise.all(Object.entries(queue.consumers).map(([topic, options]) => registrarConsumer(map, logger, queue, name, consumer, topic, options))));
}

/**
 * Queue abstrae la comunicaciÃ³n asÃ­ncrona entre microservicios usando kafka como transporter
 * 
 * TODO: En un futuro deberÃ­amos poder manejar diferente transporters (KAFKA, SQS, ETC)
 * 
 * Para usar Queue:
 * 
 * 1. Agrega la configuraciÃ³n de los tÃ³picos en tu config
 * 
 * [queue]
 * brokers = ["localhost:9092"]

 * [queue.consumers.events]
 * fromBeginning = false
 * 
 * 2. Configura queue
 * 
 * import { Queue } from "@culqi/bootstrap";
 * import config from "./config";
 * import loggerFactory from "./logger-factory";
 * import healthCheck from "./health-check";
 * 
 * export default new Queue(config, loggerFactory, healthCheck);
 * 
 * 3. Registra la acciÃ³n a ejecutar en caso de recibir un evento ie.events/ms.events.ts
 * 
 * queue.on(Topics.WEBHOOK_EVENTS, async ({ event, content: { meta, data } }) => {
 *   const { data: serviceData } = await EventServices.sendDataToWebhookURL(data);
 *   logger.info(serviceData);
 * });
 * 
 */
export class Queue {
  private _em = new EventEmitter();
  private _map = new Map<string, Array<(data: any) => void>>();
  private _emx = new EventEmitter();

  /**
   * Configura Queue y carga todos los producer y consumers definidos en la configuraciÃ³n
   *
   * @param config
   * @param loggerFactory
   * @param healthCheck
   */
  constructor(private config: Config, private loggerFactory: LoggerFactory, private healthCheck: HealthCheck) {
    Object.keys(config.me.queue).forEach((name) => this.setup(name, config.me.queue[name]));
  }

  private setup(name: string, queue: cfg.Queue): void {
    let kafka = null;

    if (!queue.ssl?.certRootPath) {
      kafka = new Kafka({
        clientId: `client:${name}:${this.config.me.application.serviceId}`,
        brokers: queue.brokers,
        ssl: queue.ssl || false,
      });
    } else {
      const { certRootPath, ca, key, cert, caEnvVar } = queue.ssl;

      if (queue.ssl?.caEnvVar) {
        process.env[`${caEnvVar}`] = `${path.join(certRootPath, ca)}`;
      }

      kafka = new Kafka({
        clientId: `client:${name}:${this.config.me.application.serviceId}`,
        brokers: queue.brokers,
        ssl: {
          rejectUnauthorized: false,
          ca: [fs.readFileSync(`${path.join(certRootPath, ca)}`, "utf-8")],
          key: fs.readFileSync(`${path.join(certRootPath, key)}`, "utf-8"),
          cert: fs.readFileSync(`${path.join(certRootPath, cert)}`, "utf-8"),
        },
      });
    }

    queue.producers && buildProducers(kafka, this.config, queue, name, this.loggerFactory, this.healthCheck, this._em, this._emx);
    queue.consumers && buildConsumers(kafka, this.config, queue, name, this.loggerFactory, this.healthCheck, this._map, this._emx);
  }

  emit(topic: string, data: { event: string; request: cfg.MetaRequest; data: Record<string, any> }) {
    this._em.emit(topic, data);
  }

  on(topic: string, fn: (data: Data) => void) {
    if (EmitterExternalEventsAsArray.find((eeeaa) => topic.endsWith(eeeaa))) {
      debug(`📡 registrando/consumer -> ${topic}`);
      this._emx.on(topic, fn);
      return;
    }

    debug(`📡 registrando/consumer -> ${topic}`);
    if (this._map.has(topic)) {
      this._map.get(topic).push(fn);
    } else {
      this._map.set(topic, [fn]);
    }
  }
}
