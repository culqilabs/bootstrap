/* eslint-disable no-console */
import debugLib from "debug";
import toml from "@iarna/toml";
import fs from "fs";
import path from "path";
import convict from "convict";
import merge from "deepmerge";
import AWS from "aws-sdk";
import ejs from "ejs";

const debug = debugLib("bootstrap:load");

const config = convict({
  env: {
    format: ["local", "test", "development", "qa", "production"],
    default: "local",
    env: "NODE_ENV",
  },
  default: {
    format: String,
    default: "local.toml",
  },
  template: {
    format: String,
    default: "template.ejs",
  },
});

function readFile(path: string): Promise<string> {
  return fs.promises.readFile(path, { encoding: "utf8" });
}

async function readToml(path: string): Promise<toml.JsonMap> {
  return toml.parse(await readFile(path));
}

function readFromBootstrap(): Promise<toml.JsonMap> {
  return readToml(path.join(__dirname, "..", "config", config.get("default")));
}

function readFromProject(): Promise<toml.JsonMap> {
  return readToml(path.join(path.resolve("config"), config.get("default")));
}

function readTemplate(): Promise<string> {
  return readFile(path.join(path.resolve("config"), config.get("template")));
}

async function readSecretManager(): Promise<Record<string, unknown>> {
  AWS.config.update({
    region: process.env.NODE_REG,
  });
  const secretsmanager = new AWS.SecretsManager();
  const credentialsData = await secretsmanager.getSecretValue({ SecretId: process.env.NODE_SEC }).promise();
  debug("<-");
  debug(credentialsData);
  debug("->");
  return JSON.parse(credentialsData.SecretString ?? "{}");
}

async function getDefault(base: Record<string, unknown>): Promise<Record<string, unknown>> {
  const project = await readFromProject();
  return merge(base, project);
}

async function getNoDefault(base: Record<string, unknown>): Promise<Record<string, unknown>> {
  const template = await readTemplate();
  const secretManager = await readSecretManager();
  const compiled = await ejs.render(template, secretManager, { async: true });
  debug("<-");
  debug(compiled);
  debug("->");
  return merge(base, toml.parse(compiled));
}

export async function load(): Promise<Record<string, unknown>> {
  const base = await readFromBootstrap();
  return config.get("env") === "local" ? getDefault(base) : getNoDefault(base);
}
