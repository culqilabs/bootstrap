import Redis from "ioredis";
import { Connection } from "mongoose";
import nanoid from "nanoid";
import { Config } from "./config";
import { HealthCheck } from "./health-check";
import ioredis from "./helpers/ioredis";
import mongoose from "./helpers/mongoose";
import { LoggerFactory } from "./logger-factory";

/**
 * Datasource configura la fuente de datos tanto para mongoose como redis,
 * dependiendo de la configuración de ambiente.
 *
 * Tanto para mongoose como redis, se permite crear más de una conexión.
 * Adicionalmente se registra cada una con el healthCheck proporcionado.
 *
 * Para Kafka, ver "events.ts"
 *
 * @param config ver tipo de dato Config en "config.d.ts"
 * @param loggerFactory
 * @param healthCheck
 */
export class Datasource {
  private id: string;
  private _mongoose: {
    [name: string]: Connection;
  };
  private _ioredis: {
    [name: string]: Redis.Redis;
  };

  constructor(private config: Config, private loggerFactory: LoggerFactory, private healthCheck?: HealthCheck) {
    this.id = nanoid();

    // mongoose
    if (this.config.me.mongoose) {
      this._mongoose = {};
      let name;

      // Crear todas las conexiones para los distintos URIs de mongoose
      Object.entries(this.config.me.mongoose).reduce((acc, [k, v]) => {
        name = `${this.id}:mongoose:${k}`;
        if (healthCheck) {
          healthCheck.registerService(name);
        }
        this._mongoose[k] = mongoose(v, this.loggerFactory, name, this.healthCheck);
        return acc;
      }, this._mongoose);
    }

    // Crear todas las conexiones para los distintos URIs de redis
    if (this.config.me.ioredis) {
      this._ioredis = {};
      let name;
      Object.entries(this.config.me.ioredis).reduce((acc, [k, v]) => {
        name = `${this.id}:ioredis:${k}`;
        if (healthCheck) {
          healthCheck.registerService(name);
        }
        this._ioredis[k] = ioredis(v, this.loggerFactory, name, this.healthCheck);
        return acc;
      }, this._ioredis);
    }
    // TODO: falta kafka aqui... Ahora se está dando preferencia a queue en vez de events.ts
  }

  get mongoose() {
    return this._mongoose;
  }

  get ioredis() {
    return this._ioredis;
  }
}
