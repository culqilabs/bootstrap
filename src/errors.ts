import defaults from "defaults";
// Clases de excepciones que son manejadas y entendidas por nuestro app.ts
// (el middleware en app.ts las maneja acordemente.)

export type httpStatusCodeError = 511 | 507 | 505 | 504 | 503 | 502 | 501 | 500 | 431 | 429 | 428 | 424 | 423 | 422 | 420 | 419 | 418 | 417 | 416 | 415 | 414 | 413 | 412 | 411 | 410 | 409 | 408 | 407 | 406 | 405 | 404 | 403 | 402 | 401 | 400;

export class BusinessError extends Error {
  readonly phrase: string;
  readonly status: number;
  readonly options: Readonly<{
    replacements?: Readonly<Record<string, unknown>>;
    translate?: boolean;
    expose?: boolean;
  }>;

  constructor(
    phrase: string,
    status: number,
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean;
      expose?: boolean;
    },
  ) {
    const $options = defaults(options, { replacements: {}, translate: true, expose: false });
    super(phrase);
    this.phrase = phrase;
    this.status = status;
    this.options = $options;
  }
}

export class InternalError extends Error {
  readonly phrase: string;
  readonly status: httpStatusCodeError;
  readonly options: Readonly<{
    replacements?: Readonly<Record<string, unknown>>;
    translate?: boolean;
    expose?: boolean;
  }>;

  constructor(
    phrase: string,
    status: httpStatusCodeError,
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean;
      expose?: boolean;
    },
  ) {
    const $options = defaults(options, { replacements: {}, translate: true, expose: false });
    super(phrase);
    this.phrase = phrase;
    this.status = status;
    this.options = $options;
  }
}

export class ExposeError extends Error {
  readonly phrase: Readonly<Record<string, unknown>>;
  readonly status: httpStatusCodeError;
  readonly options: Readonly<{
    replacements?: Readonly<Record<string, unknown>>;
    translate?: boolean;
    expose?: boolean;
  }>;

  constructor(
    phrase: Record<string, unknown>,
    status: httpStatusCodeError,
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean;
      expose?: boolean;
    },
  ) {
    const $options = defaults(options, { replacements: {}, translate: true, expose: true });
    super(JSON.stringify(phrase));
    this.phrase = phrase;
    this.status = status;
    this.options = $options;
  }
}

export class InvalidArgumentError extends Error {
  readonly phrase: string;
  readonly status: httpStatusCodeError;
  readonly options: Readonly<{
    replacements?: Readonly<Record<string, unknown>>;
    translate?: boolean;
    expose?: boolean;
  }>;

  constructor(
    phrase: string,
    status: httpStatusCodeError,
    options?: {
      replacements?: Readonly<Record<string, unknown>>;
      translate?: boolean;
      expose?: boolean;
    },
  ) {
    const $options = defaults(options, { replacements: {}, translate: true, expose: false });
    super(phrase);
    this.phrase = phrase;
    this.status = status;
    this.options = $options;
  }
}
