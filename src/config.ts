import debugLib from "debug";
import mergeOptions from "merge-options";
import path from "path";
import toml from "./helpers/toml";
import * as cfg from "./types/config";

const debug = debugLib("bootstrap:cfg");

/**
 * Cargar configuración "local" de este mismo proyecto (bootstrap)
 */
function loadDefaultConfigFromPackage() {
  debug("loadDefaultConfigFromPackage: local");
  return toml(path.join(__dirname, "..", "config"))("local");
}

/**
 * Cargar configuración "local" del directorio /config del microservicio
 */
function loadConfigFromProject() {
  debug(`loadConfigFromProject: ${process.env.NODE_ENV}`);
  return toml(path.resolve("config"))(process.env.NODE_ENV, "local");
}

/**
 * Clase que gestiona la configuración de ambientes de los microservicios.
 *
 * Se carga la configuración que está en local.toml de la libreria bootstrap,
 * y se le hace "merge" con las opciones de configuración de tu proyecto actual,
 * usando NODE_ENV para escoger el envirnoment (local, etc.).
 *
 * Si se proporciona el parametro config, la configuración se obtiene de allí, y no de los archivos .toml del microservicio.
 * @param config
 */
export class Config {
  private _me: cfg.Config;

  constructor(private config?: cfg.Config) {
    this._me = mergeOptions(loadDefaultConfigFromPackage(), this.config || loadConfigFromProject());
    debug(`mergedConfig: ${JSON.stringify(this._me)}`);
  }

  /**
   * Obtener configuración actual en memoria.
   */
  get me() {
    debug(`mergedConfig ME: ${JSON.stringify(this._me)}`);
    return this._me;
  }
}
