import debugLib from "debug";
import Redis from "ioredis";
import mergeOptions from "merge-options";
import { HealthCheck } from "../health-check";
import { LoggerFactory } from "../logger-factory";
import * as cfg from "../types/config";

const debug = debugLib("bootstrap:hc");

const parse = (config: cfg.Ioredis): cfg.Ioredis => {
  config = mergeOptions(
    {
      options: {},
    },
    config,
  );
  return config;
};

/**
 * Crea conexion a Redis, inyecta el healthCheck si sea necesario, y loguea errores en loggerFactory.
 */
export default (config: cfg.Ioredis, loggerFactory: LoggerFactory, name: string, healthCheck?: HealthCheck): Redis.Redis => {
  const loggerApp = loggerFactory.getLogger("app");
  const loggerShutdown = loggerFactory.getLogger("shutdown");

  config = parse(config);
  debug(`ioredis:${name} -> ${JSON.stringify(config)}`);
  const connection = new Redis(config.uri);

  if (healthCheck) {
    connection.on("ready", () => {
      debug(`ready > ${name}`);
      healthCheck.emit(name, true);
    });
    connection.on("error", (err: Error) => {
      debug(`error > ${name}`);
      loggerApp.error(err);
      healthCheck.emit(name, false);
    });
    connection.on("close", () => {
      debug(`close > ${name}`);
      healthCheck.emit(name, false);
    });
    healthCheck.registerShutdown(() => {
      loggerShutdown.info(`shutdown connection: ${name}`);
      return connection.quit();
    });
  }
  return connection;
};
