import debugLib from "debug";
import { Consumer, Kafka, Producer } from "kafkajs";
import mergeOptions from "merge-options";
import { HealthCheck } from "../health-check";
import { LoggerFactory } from "../logger-factory";
import * as cfg from "../types/config";

const debug = debugLib("bootstrap:hc");

const parse = (config: cfg.Kafkajs): cfg.Kafkajs => mergeOptions({ options: {} }, config);

export const getKafka = (config: cfg.Kafkajs): Kafka => {
  config = parse(config);
  const connection = new Kafka(config);

  return connection;
};

/**
 * @deprecated Usar Queue en vez.
 * 
 * getProducer devuelve un productor de kafka con logging y healtCheck
 * 
 * @param kafka 
 * @param loggerFactory 
 * @param name Nombre de la conexión, para logging y healthCheck.
 * @param healthCheck 

 */
export const getProducer = (kafka: Kafka, loggerFactory: LoggerFactory, name: string, healthCheck?: HealthCheck): Producer => {
  const producer = kafka.producer();
  producer.connect();
  const { CONNECT, DISCONNECT } = producer.events;

  if (healthCheck) {
    producer.on(CONNECT, () => {
      debug(`${CONNECT} > ${name}`);
      healthCheck.emit(name, true);
    });
    producer.on(DISCONNECT, () => {
      debug(`${DISCONNECT} > ${name}`);
      healthCheck.emit(name, false);
    });
  }
  return producer;
};

/**
 * @deprecated Usar Queue en vez.
 *
 * getConsumer devuelve un consumidor de kafka con logging y healtCheck
 *
 * @param kafka
 * @param config
 * @param loggerFactory
 * @param name  Nombre de la conexión, para logging y healthCheck.
 * @param healthCheck
 */

export const getConsumer = (kafka: Kafka, config: cfg.KafkaConsumerConfig, loggerFactory: LoggerFactory, name: string, healthCheck?: HealthCheck): Consumer => {
  const consumer = kafka.consumer(config);
  consumer.connect();
  const { CONNECT, DISCONNECT, HEARTBEAT, CRASH, STOP, REQUEST_TIMEOUT } = consumer.events;

  const loggerApp = loggerFactory.getLogger("app");
  const loggerShutdown = loggerFactory.getLogger("shutdown");

  if (healthCheck) {
    consumer.on(CONNECT, () => {
      debug(`${CONNECT} > ${name}`);
      healthCheck.emit(name, true);
    });
    consumer.on(DISCONNECT, () => {
      debug(`${DISCONNECT} > ${name}`);
      healthCheck.emit(name, false);
    });
    consumer.on(HEARTBEAT, () => {
      debug(`${HEARTBEAT} > ${name}`);
      healthCheck.emit(name, true);
    });
    consumer.on(CRASH, (err: Error, groupId: string) => {
      debug(`${CRASH} > ${name}-groupId=${groupId}`);
      loggerApp.error(err);
      healthCheck.emit(name, false);
    });
    consumer.on(STOP, () => {
      debug(`${STOP} > ${name}`);
      healthCheck.emit(name, false);
    });
    consumer.on(REQUEST_TIMEOUT, () => {
      debug(`${REQUEST_TIMEOUT} > ${name}`);
      healthCheck.emit(name, false);
    });

    healthCheck.registerShutdown(() => {
      loggerShutdown.info(`shutdown connection: ${name}`);
      return consumer.disconnect();
    });
  }

  config.topics.forEach((topic: string) => {
    loggerApp.info(`subscribing to topic=${topic}`);
    consumer.subscribe({ topic });
  });

  return consumer;
};
