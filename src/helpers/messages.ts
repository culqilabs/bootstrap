import moment from "moment-timezone";
import errorTypes from "../helpers/error-types";
import * as cfg from "../types/config";

export const getMeta = (config: cfg.Application, request: cfg.MetaRequest): cfg.Meta => ({
  serviceId: config.serviceId,
  timestamp: moment()
    .tz(config.timezone)
    .format(config.dateFormat),
  request,
});

export const success = (config: cfg.Application, request: cfg.MetaRequest, data: any, sessionId?: string): cfg.GenericSuccess => ({
  meta: { ...getMeta(config, request), sessionId },
  data,
});

export const error = (config: cfg.Application, request: cfg.MetaRequest, message: string | Record<string, unknown>, status: number, type: errorTypes, sessionId?: string): cfg.GenericError => ({
  meta: { ...getMeta(config, request), sessionId },
  error: {
    message,
    status,
    type,
  },
});

export const businessError = (config: cfg.Application, request: cfg.MetaRequest, message: string, status: number, sessionId?: string): cfg.GenericError => error(config, request, message, status, errorTypes.BUSINESS, sessionId);

export const internalError = (config: cfg.Application, request: cfg.MetaRequest, message: string, status: number, sessionId?: string): cfg.GenericError => error(config, request, message, status, errorTypes.INTERNAL, sessionId);

export const exposeError = (config: cfg.Application, request: cfg.MetaRequest, message: Record<string, unknown>, status: number, sessionId?: string): cfg.ExposeError => error(config, request, message, status, errorTypes.EXPOSE, sessionId);

export const invalidArgumentError = (config: cfg.Application, request: cfg.MetaRequest, message: string | Record<string, unknown>, status: number, sessionId?: string): cfg.ExposeError => error(config, request, message, status, errorTypes.INVALID_ARGUMENT, sessionId);
