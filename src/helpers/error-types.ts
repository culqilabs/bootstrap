enum ErrorTypes {
  BUSINESS = "business",
  INTERNAL = "internal",
  EXPOSE = "expose",
  INVALID_ARGUMENT = "invalidArgument",
}

export default ErrorTypes;
