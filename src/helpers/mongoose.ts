import debugLib from "debug";
import fs from "fs";
import mergeOptions from "merge-options";
import mongoose, { Connection } from "mongoose";
import path from "path";
import { HealthCheck } from "../health-check";
import { LoggerFactory } from "../logger-factory";
import * as cfg from "../types/config";

const debug = debugLib("bootstrap:hc");

const parse = (config: cfg.Mongoose): cfg.Mongoose => {
  config = mergeOptions(
    {
      options: {
        useFindAndModify: false,
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        bufferCommands: false,
        keepAlive: true,
        keepAliveInitialDelay: 5000,
        // reconnectTries: 60,
        // reconnectInterval: 1000,
        ssl: false,
        sslValidate: false,
      },
    },
    config,
  );

  if (config?.options?.sslCA) {
    config.options.sslCA = config.options.sslCA.map((item: string) => fs.readFileSync(path.resolve(item), "utf8"));
  }

  return config;
};

/**
 * Crea una conexión a mongoose, inyecta el healthCheck, y loguea errores en caso los hubiere.
 *
 * @param config
 * @param loggerFactory
 * @param name
 * @param healthCheck
 *
 */
export default (config: cfg.Mongoose, loggerFactory: LoggerFactory, name: string, healthCheck?: HealthCheck): Connection => {
  // logger
  const loggerApp = loggerFactory.getLogger("app");
  // logger
  const loggerShutdown = loggerFactory.getLogger("shutdown");
  config = parse(config);
  debug(`mongoose:${name} -> ${JSON.stringify(config)}`);
  const connection = mongoose.createConnection(config.uri, config.options);
  if (healthCheck) {
    connection.on("open", () => {
      debug(`open > ${name}`);
      healthCheck.emit(name, true);
    });
    connection.on("disconnected", () => {
      debug(`disconnected > ${name}`);
      healthCheck.emit(name, false);
    });
    connection.on("close", () => {
      debug(`close > ${name}`);
      healthCheck.emit(name, false);
    });
    connection.on("reconnected", () => {
      debug(`reconnected > ${name}`);
      healthCheck.emit(name, true);
    });
    connection.on("error", (err: Error) => {
      debug(`error > ${name}`);
      loggerApp.error(err);
      healthCheck.emit(name, false);
    });
    connection.on("reconnectFailed", () => {
      debug(`reconnectFailed > ${name}`);
      healthCheck.emit(name, false);
    });
    healthCheck.registerShutdown(() => {
      loggerShutdown.info(`shutdown connection: ${name}`);
      return connection.close();
    });
  }
  return connection;
};
