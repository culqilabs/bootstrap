import { Log4js } from "log4js";
import { Config } from "./config";
import log4js from "./plugins/log4js";

/**
 * Crea un Log4js basado en Config.
 */
export class LoggerFactory {
  private _logger: Log4js;

  constructor(private config: Config) {
    this._logger = log4js(this.config.me);
  }

  getLogger(category: string) {
    return this._logger.getLogger(category);
  }
}
