import cors from "cors";
import defaults from "defaults";
import { EventEmitter } from "events";
import express, { Application, Response } from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import "express-async-errors";
import { createRouter, Router } from "express-laravel-router";
import http from "http";
import { Config } from "./config";
import { BusinessError, InternalError, ExposeError, InvalidArgumentError } from "./errors";
import { HealthCheck } from "./health-check";
import * as messages from "./helpers/messages";
import { LoggerFactory } from "./logger-factory";
import i18n, { nestedReplacements } from "./plugins/i18n";
import meta from "./plugins/meta";
import * as cfg from "./types/config";

/*
 * Funciones utilitarias que se agregan al objeto response para estandarizar
 * los mensajes de respuesta que los microservicios puede responder.
 */
express.response.$success = function(data) {
  this.app.locals.$ee.emit("success", this, data);
};

express.response.$businessError = function({ message, status, options }) {
  this.app.locals.$ee.emit("businessError", this, message, status, options);
};

express.response.$internalError = function({ message, status, options }) {
  this.app.locals.$ee.emit("internalError", this, message, status, options);
};

express.response.$exposeError = function({ message, status, options }) {
  this.app.locals.$ee.emit("exposeError", this, message, status, options);
};

express.response.$invalidArgumentError = function({ message, status, options }) {
  this.app.locals.$ee.emit("invalidArgumentError", this, message, status, options);
};

express.response.$logTrace = function(message: any) {
  this.app.locals.$ee.emit("logTrace", this, message);
};

express.response.$logDebug = function(message: any) {
  this.app.locals.$ee.emit("logDebug", this, message);
};

express.response.$logInfo = function(message: any) {
  this.app.locals.$ee.emit("logInfo", this, message);
};

express.response.$logWarn = function(message: any) {
  this.app.locals.$ee.emit("logWarn", this, message);
};

express.response.$logError = function(message: any) {
  this.app.locals.$ee.emit("logError", this, message);
};

express.response.$logFatal = function(message: any) {
  this.app.locals.$ee.emit("logFatal", this, message);
};

type OptionslOptions = {
  replacements?: Readonly<Record<string, unknown>>;
  translate?: boolean;
  expose?: boolean;
};

type Options = {
  replacements: Readonly<Record<string, unknown>>;
  translate: boolean;
  expose: boolean;
};

/**
 * Encapsula la construcción de Express y le agregar funciones útiles para los microservicios.
 */
export class App {
  private _me: Application;

  /**
   * Constructor de la clase App (encapsula a Application)
   *
   * @param config
   * @param loggerFactory
   * @param routes
   */
  constructor(private config: Config, private loggerFactory: LoggerFactory, private routes?: (router: Router) => void, private cb?: (app: express.Express) => void) {
    const logger = this.loggerFactory.getLogger("HTTP");

    // Config express
    const me = (this._me = express());
    me.use(cors());
    i18n(me, config.me.i18n.dir, config.me.i18n);
    me.use(helmet());
    me.use(bodyParser.json({ limit: "50mb" }));
    me.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    me.use(express.json());
    me.use(meta);

    const ee = new EventEmitter();

    /**
     * Funciones utilitarias para manejar logging usando el formato definido en
     *
     * https://culqilabs.gitlab.io/docs/docs/microservices/index#meta
     */
    ee.on("success", (res: Response, data: any) => {
      const $body = messages.success(config.me.application, res.locals?.meta?.request, data, res.locals?.meta?.sessionId);
      $body.meta = { ...$body.meta, originId: config.me.application.serviceId };
      logger.trace($body, $body?.meta, res.locals.originId, res.locals.path);
      res.set("x-trace-id", res.locals?.meta?.request.traceId);
      res.json($body);
    });

    // businessError
    ee.on("businessError", (res: Response, message: string, status = 400, options: OptionslOptions) => {
      const $options: Options = defaults(options, { replacements: {}, translate: true, expose: false });
      const msg = $options.translate ? (res.$t(message, $options.replacements) as string) : message;
      const $body = messages.businessError(config.me.application, res.locals?.meta?.request, msg, status, res.locals?.meta?.sessionId);
      $body.meta = { ...$body.meta, originId: config.me.application.serviceId };
      logger.trace($body, $body?.meta, res.locals.originId, res.locals.path);
      res.set("x-status-code", `${status}`);
      res.set("x-trace-id", res.locals?.meta?.request.traceId);
      res.json($body);
    });

    // internalError
    ee.on("internalError", (res: Response, message: string, status = 500, options: OptionslOptions) => {
      const $options: Options = defaults(options, { replacements: {}, translate: true, expose: false });
      const msg = $options.translate ? (res.$t(message, $options.replacements) as string) : message;
      const $body = messages.internalError(config.me.application, res.locals?.meta?.request, msg, status, res.locals?.meta?.sessionId);
      $body.meta = { ...$body.meta, originId: config.me.application.serviceId };
      logger.trace($body, $body?.meta, res.locals.originId, res.locals.path);
      res.set("x-status-code", `${status}`);
      res.set("x-trace-id", res.locals?.meta?.request.traceId);
      res.json($body);
    });

    // exposeError
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ee.on("exposeError", (res: Response, message: Record<string, unknown>, status = 500, options: OptionslOptions) => {
      const $options: Options = defaults(options, { replacements: {}, translate: true, expose: true });
      const msg = $options.translate ? (nestedReplacements(message, $options.replacements) as Record<string, unknown>) : message;
      const $body = messages.exposeError(config.me.application, res.locals?.meta?.request, msg, status, res.locals?.meta?.sessionId);
      $body.meta = { ...$body.meta, originId: config.me.application.serviceId };
      logger.trace($body, $body?.meta, res.locals.originId, res.locals.path);
      res.set("x-status-code", `${status}`);
      res.set("x-trace-id", res.locals?.meta?.request.traceId);
      res.json($body);
    });

    // invalidArgumentError
    ee.on("invalidArgumentError", (res: Response, message: string, status = 400, options: OptionslOptions) => {
      const $options: Options = defaults(options, { replacements: {}, translate: true, expose: false });
      const msg = $options.translate ? nestedReplacements(res.$t(message, $options.replacements), $options.replacements) : message;
      let $body: cfg.ExposeError;
      if ($options.expose) {
        $body = messages.exposeError(config.me.application, res.locals?.meta?.request, typeof msg === "string" ? { message: msg } : msg, status, res.locals?.meta?.sessionId);
      } else {
        $body = messages.invalidArgumentError(config.me.application, res.locals?.meta?.request, msg, status, res.locals?.meta?.sessionId);
      }
      $body.meta = { ...$body.meta, originId: config.me.application.serviceId };
      logger.trace($body, $body?.meta, res.locals.originId, res.locals.path);
      res.set("x-status-code", `${status}`);
      res.set("x-trace-id", res.locals?.meta?.request.traceId);
      res.json($body);
    });

    ee.on("logTrace", (res: Response, message: any) => logger.trace(message, res.locals?.meta));
    ee.on("logDebug", (res: Response, message: any) => logger.debug(message, res.locals?.meta));
    ee.on("logInfo", (res: Response, message: any) => logger.info(message, res.locals?.meta));
    ee.on("logWarn", (res: Response, message: any) => logger.warn(message, res.locals?.meta));
    ee.on("logError", (res: Response, message: any) => logger.error(message, res.locals?.meta));
    ee.on("logFatal", (res: Response, message: any) => logger.fatal(message, res.locals?.meta));
    me.locals.$ee = ee;

    // Middleware para los requests, agregandole el originId y path a locals, así como
    // escribiendo en el log con level TRACE.

    me.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
      res.locals.originId = req.body?.meta?.originId;
      res.locals.path = req.originalUrl;
      logger.trace(req.body, req.body?.meta, res.locals.originId, res.locals.path);
      next();
    });

    // Registra los endpoints definidos en el microservicio
    this.routes && this.routes(createRouter(me));

    // Callback
    this.cb && this.cb(me);

    // route notfound --> 404 error y logging
    me.use((req: express.Request, res: express.Response) => {
      if (process.env.NODE_ENV !== "production") {
        console.error(`Ruta no encontrada: ${req.path}`);
      }
      return res.$invalidArgumentError({
        message: "generic.errors.pathNotFound",
        options: {
          replacements: { path: req.path },
        },
        status: 404,
      });
    });

    /**
     * Middleware de error handling:
     *
     * builtin handling para BusinessError e InternalError
     * Cualquier otro caso será HTTP 500 y error sacado de "generic.errors.internalServer"
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    me.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
      if (process.env.NODE_ENV !== "production") {
        console.error(err);
      }
      if (err instanceof BusinessError) {
        const error = err as BusinessError;
        return res.$businessError({
          message: error.phrase,
          status: error.status,
          options: error.options,
        });
      }
      if (err instanceof InternalError) {
        const error = err as InternalError;
        return res.$internalError({
          message: error.phrase,
          status: error.status,
          options: error.options,
        });
      }
      if (err instanceof ExposeError) {
        const error = err as ExposeError;
        return res.$exposeError({
          message: error.phrase,
          status: error.status,
          options: error.options,
        });
      }
      if (err instanceof InvalidArgumentError) {
        const error = err as InvalidArgumentError;
        return res.$invalidArgumentError({
          message: error.phrase,
          status: error.status,
          options: error.options,
        });
      }
      res.$logError({ message: err.message, status: 500, stack: err.stack });
      return res.$internalError({
        message: "generic.errors.internalServer",
        status: 500,
      });
    });
  }

  /**
   * Inicio del server, inyecta el healthcheck.
   * @param healthCheck
   */
  start(healthCheck?: HealthCheck): void {
    const logger = this.loggerFactory.getLogger("startup");
    const server = http.createServer(this._me);
    const port = this.config.me?.application?.port ?? 3000;

    if (healthCheck) {
      healthCheck.listen(server);
    }

    server.listen(port, () => logger.info(`🚀  server running on port ${port}`));
  }

  /**
   * Retorna el objeto Application original, encapsulado por éste.
   */
  get me() {
    return this._me;
  }
}
